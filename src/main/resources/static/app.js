var stompClient = null;
var maxConnectionRetry = 5;
var connectionRetryCount = 0;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    $("#user_notification_updates").html("");
    if (connected) {
        $("#connection_status").text('Connected!')
            .addClass("text-success")
            .removeClass("text-muted")
            .removeClass("text-danger");
        $("#user_notification_updates").append("<tr><td>When connected, User notifications will show up here...</td></tr>");
    } else {
        $("#connection_status").text('Disconnected!')
            .addClass("text-danger")
            .removeClass("text-muted")
            .removeClass("text-success");
        $("#user_notification_updates").append("<tr><td>When connected, User notifications will show up here...</td></tr>");
    }
}

function connect() {
    var notificationPreferences = document.getElementById("notification_preferences").value;
    var accessToken = document.getElementById("access_token").value;
    var clientId = JSON.parse(atob(accessToken.split('.')[1])).azp;
    var userReference = JSON.parse(atob(accessToken.split('.')[1])).user_reference;
    var endpoint = window.location.protocol + "//" + window.location.host
        + "/user-notifications/connect?device_id=" + clientId
        + "&user_reference=" + userReference
        + "&notification_preferences=" + notificationPreferences
        + "&x-angularpay-user-agent=AngularPay-Test-Websocket-UN-Client-v1.0-M51316"
        + "&access_token=" + accessToken;

    var socket = new SockJS(endpoint);
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        connectionRetryCount = 0;
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/topic/all-notifications', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investment-verified', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/solo-investor-added', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/peer-investor-added', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investor-bargain-added', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/bargain-accepted', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/bargain-rejected', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investor-deleted-by-ttl', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investor-deleted-by-self', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investment-completed', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investment-mature', function (message) {
            onMessage(JSON.parse(message.body));
        });
        stompClient.subscribe('/user/topic/investment-cancelled', function (message) {
            onMessage(JSON.parse(message.body));
        });
    }, function (message) {
        console.log('--------------- errorCallback ------------');
        console.log(message);
        if (message && message.includes("Whoops! Lost connection to")) {
            if (connectionRetryCount <= maxConnectionRetry) {
                setTimeout(() => {
                    console.log('--------------- retrying connection ------------');
                    ++connectionRetryCount;
                    connect();
                }, 1000);
            } else {
                connectionRetryCount = 0;
            }
        }
    });
}

function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function onMessage(message) {
    $("#user_notification_updates").append("<tr><td>" + JSON.stringify(message) + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $("#connect").click(function () {
        connect();
    });
    $("#disconnect").click(function () {
        disconnect();
    });
});


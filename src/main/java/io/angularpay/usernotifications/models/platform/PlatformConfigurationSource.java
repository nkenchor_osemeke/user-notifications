
package io.angularpay.usernotifications.models.platform;

import lombok.Getter;

@Getter
public enum PlatformConfigurationSource {
    HASH, TOPIC
}

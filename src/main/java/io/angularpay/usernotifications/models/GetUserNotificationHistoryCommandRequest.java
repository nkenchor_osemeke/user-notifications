package io.angularpay.usernotifications.models;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@SuperBuilder
public class GetUserNotificationHistoryCommandRequest extends AccessControl {

    GetUserNotificationHistoryCommandRequest(AuthenticatedUser authenticatedUser) {
        super(authenticatedUser);
    }
}

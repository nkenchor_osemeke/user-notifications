package io.angularpay.usernotifications.helpers;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.usernotifications.adapter.outbound.MongoAdapter;
import io.angularpay.usernotifications.configurations.AngularPayConfiguration;
import io.angularpay.usernotifications.domain.UserNotificationHistory;
import io.angularpay.usernotifications.exceptions.CommandException;
import io.angularpay.usernotifications.exceptions.ErrorCode;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import static io.angularpay.usernotifications.exceptions.ErrorCode.REQUEST_NOT_FOUND;

@Service
@RequiredArgsConstructor
public class CommandHelper {

    private final MongoAdapter mongoAdapter;
    private final ObjectMapper mapper;
    private final AngularPayConfiguration configuration;

    public String getRequestOwner(String userReference) {
        UserNotificationHistory found = this.mongoAdapter.findRequestByUserReference(userReference).orElseThrow(
                () -> commandException(HttpStatus.NOT_FOUND, REQUEST_NOT_FOUND)
        );
        return found.getUserReference();
    }

    private static CommandException commandException(HttpStatus status, ErrorCode errorCode) {
        return CommandException.builder()
                .status(status)
                .errorCode(errorCode)
                .message(errorCode.getDefaultMessage())
                .build();
    }

}

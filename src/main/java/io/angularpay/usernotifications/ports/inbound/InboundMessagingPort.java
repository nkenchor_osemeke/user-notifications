package io.angularpay.usernotifications.ports.inbound;

import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.models.platform.PlatformConfigurationIdentifier;

public interface InboundMessagingPort {
    void onMessage(UserNotification userNotification);
    void onMessage(String message, PlatformConfigurationIdentifier identifier);
}

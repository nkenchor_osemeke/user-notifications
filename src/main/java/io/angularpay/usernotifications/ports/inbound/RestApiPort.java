package io.angularpay.usernotifications.ports.inbound;

import io.angularpay.usernotifications.domain.UserNotification;

import java.util.List;
import java.util.Map;

public interface RestApiPort {
    List<UserNotification> getUserNotificationHistory(Map<String, String> headers);
}

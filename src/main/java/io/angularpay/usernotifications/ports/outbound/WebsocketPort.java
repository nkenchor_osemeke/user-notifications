package io.angularpay.usernotifications.ports.outbound;

import io.angularpay.usernotifications.domain.UserNotification;

public interface WebsocketPort {
    void publishToUser(UserNotification userNotification);
}

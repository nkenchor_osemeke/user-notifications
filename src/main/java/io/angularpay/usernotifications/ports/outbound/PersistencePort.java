package io.angularpay.usernotifications.ports.outbound;

import io.angularpay.usernotifications.domain.UserNotificationHistory;

import java.util.Optional;

public interface PersistencePort {
    UserNotificationHistory createRequest(UserNotificationHistory request);
    UserNotificationHistory updateRequest(UserNotificationHistory request);
    Optional<UserNotificationHistory> findRequestByUserReference(String userReference);
}

package io.angularpay.usernotifications.services;

import io.angularpay.usernotifications.domain.UserNotificationType;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserNotificationPreferenceService {

    private final Map<String, List<UserNotificationType>> notificationPreferences = new HashMap<>();

    public void addUserPreferences(String userReference, List<UserNotificationType> preferences) {
        this.notificationPreferences.put(userReference, preferences);
    }

    public List<UserNotificationType> getUserPreferences(String userReference) {
        List<UserNotificationType> preferences = this.notificationPreferences.get(userReference);
        if (CollectionUtils.isEmpty(preferences)) {
            return Collections.emptyList();
        } else {
            return preferences;
        }
    }

    public boolean isUserPreference(String userReference, UserNotificationType type) {
        boolean allNotifications = this.getUserPreferences(userReference).stream()
                .anyMatch(x -> x == UserNotificationType.ALL_NOTIFICATIONS);
            return allNotifications || this.getUserPreferences(userReference).stream().anyMatch(x -> x == type);
    }
}

package io.angularpay.usernotifications.adapter.inbound;

import io.angularpay.usernotifications.adapter.outbound.WebsocketAdapter;
import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.domain.commands.PlatformConfigurationsConverterCommand;
import io.angularpay.usernotifications.models.platform.PlatformConfigurationIdentifier;
import io.angularpay.usernotifications.ports.inbound.InboundMessagingPort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import static io.angularpay.usernotifications.models.platform.PlatformConfigurationSource.TOPIC;

@Service
@RequiredArgsConstructor
public class RedisMessageAdapter implements InboundMessagingPort {

    private final WebsocketAdapter websocketAdapter;
    private final PlatformConfigurationsConverterCommand converterCommand;

    @Override
    public void onMessage(UserNotification userNotification) {
        websocketAdapter.publishToUser(userNotification);
    }

    @Override
    public void onMessage(String message, PlatformConfigurationIdentifier identifier) {
        this.converterCommand.execute(message, identifier, TOPIC);
    }
}

package io.angularpay.usernotifications.adapter.inbound;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.domain.UserNotificationType;
import io.angularpay.usernotifications.domain.commands.AddUserNotificationHistoryCommand;
import io.angularpay.usernotifications.exceptions.CommandException;
import io.angularpay.usernotifications.models.AddUserNotificationHistoryCommandRequest;
import io.angularpay.usernotifications.models.AuthenticatedUser;
import io.angularpay.usernotifications.services.UserNotificationPreferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.connection.MessageListener;
import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

import java.util.Objects;

import static io.angularpay.usernotifications.exceptions.ErrorCode.*;

@Slf4j
@RequiredArgsConstructor
public class UserNotificationMessageSubscriber implements MessageListener {

    private final RedisMessageAdapter redisMessageAdapter;
    private final UserNotificationPreferenceService userNotificationPreferenceService;
    private final AddUserNotificationHistoryCommand addUserNotificationHistoryCommand;
    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void onMessage(Message message, byte[] bytes) {
        try {
            String messageJson = String.valueOf(message);
            UserNotification userNotification;
            try {
                userNotification = mapper.readValue(messageJson, UserNotification.class);
            } catch (Exception exception) {
                throw CommandException.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .errorCode(INVALID_MESSAGE_JSON)
                        .message(INVALID_MESSAGE_JSON.getDefaultMessage())
                        .build();
            }

            if (!StringUtils.hasText(userNotification.getUserReference())) {
                throw CommandException.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .errorCode(INVALID_USER_REFERENCE_ERROR)
                        .message(INVALID_USER_REFERENCE_ERROR.getDefaultMessage())
                        .build();
            }

            if (Objects.isNull(userNotification.getType())) {
                throw CommandException.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .errorCode(INVALID_NOTIFICATION_TYPE_ERROR)
                        .message(INVALID_NOTIFICATION_TYPE_ERROR.getDefaultMessage())
                        .build();
            }

            if (userNotification.getType() == UserNotificationType.ALL_NOTIFICATIONS) {
                throw CommandException.builder()
                        .status(HttpStatus.INTERNAL_SERVER_ERROR)
                        .errorCode(ILLEGAL_NOTIFICATION_TYPE_ERROR)
                        .message(ILLEGAL_NOTIFICATION_TYPE_ERROR.getDefaultMessage())
                        .build();
            }

            boolean isUserPreference = this.userNotificationPreferenceService.isUserPreference(
                    userNotification.getUserReference(),
                    userNotification.getType());

            if (isUserPreference) {
                log.info("saving {} notification to user {} notification history", userNotification.getType(), userNotification.getUserReference());
                AuthenticatedUser authenticatedUser = AuthenticatedUser.builder()
                        .userReference(userNotification.getUserReference())
                        .username(userNotification.getUserReference() + "-not-actual-username")
                        .deviceId(userNotification.getUserReference() + "-not-actual-device-id")
                        .build();

                AddUserNotificationHistoryCommandRequest addUserNotificationHistoryCommandRequest = AddUserNotificationHistoryCommandRequest.builder()
                        .authenticatedUser(authenticatedUser)
                        .userNotification(userNotification)
                        .build();
                this.addUserNotificationHistoryCommand.execute(addUserNotificationHistoryCommandRequest);

                log.info("sending {} notification to user {}", userNotification.getType(), userNotification.getUserReference());
                this.redisMessageAdapter.onMessage(userNotification);
            } else {
                log.info("{} notification type is not in user's - {} - notification preferences", userNotification.getType(), userNotification.getUserReference());
            }
        } catch (Exception exception) {
            log.error("An error occurred while processing user-notifications request", exception);
        }
    }
}

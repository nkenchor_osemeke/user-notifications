package io.angularpay.usernotifications.adapter.inbound;

import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.domain.commands.GetUserNotificationHistoryCommand;
import io.angularpay.usernotifications.models.AuthenticatedUser;
import io.angularpay.usernotifications.models.GetUserNotificationHistoryCommandRequest;
import io.angularpay.usernotifications.ports.inbound.RestApiPort;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

import static io.angularpay.usernotifications.helpers.Helper.fromHeaders;

@RestController
@RequestMapping("/user-notifications/history")
@RequiredArgsConstructor
public class RestApiAdapter implements RestApiPort {

    private final GetUserNotificationHistoryCommand getUserNotificationHistoryCommand;

    @GetMapping
    @Override
    public List<UserNotification> getUserNotificationHistory(@RequestHeader Map<String, String> headers) {
        AuthenticatedUser authenticatedUser = fromHeaders(headers);
        GetUserNotificationHistoryCommandRequest getUserNotificationHistoryCommandRequest = GetUserNotificationHistoryCommandRequest.builder()
                .authenticatedUser(authenticatedUser)
                .build();
        return getUserNotificationHistoryCommand.execute(getUserNotificationHistoryCommandRequest);
    }
}

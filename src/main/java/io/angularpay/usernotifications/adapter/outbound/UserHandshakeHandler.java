package io.angularpay.usernotifications.adapter.outbound;

import com.sun.security.auth.UserPrincipal;
import io.angularpay.usernotifications.domain.UserNotificationType;
import io.angularpay.usernotifications.exceptions.CommandException;
import io.angularpay.usernotifications.services.UserNotificationPreferenceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.util.StringUtils;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import java.security.Principal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static io.angularpay.usernotifications.exceptions.ErrorCode.*;

@Slf4j
@RequiredArgsConstructor
public class UserHandshakeHandler extends DefaultHandshakeHandler {

    private final UserNotificationPreferenceService userNotificationPreferenceService;

    @Override
    protected Principal determineUser(ServerHttpRequest request, WebSocketHandler wsHandler, Map<String, Object> attributes) {
        try {
            String queryString = request.getURI().getQuery();
            if (!StringUtils.hasText(queryString)) {
                throw CommandException.builder()
                        .status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .errorCode(NO_PRINCIPAL)
                        .message(NO_PRINCIPAL.getDefaultMessage())
                        .build();
            }

            if (!queryString.contains("user_reference")) {
                throw CommandException.builder()
                        .status(HttpStatus.FORBIDDEN)
                        .errorCode(NO_PRINCIPAL)
                        .message(NO_PRINCIPAL.getDefaultMessage())
                        .build();
            }

            if (!queryString.contains("notification_preferences")) {
                throw CommandException.builder()
                        .status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .errorCode(INVALID_NOTIFICATION_PREFERENCE_ERROR)
                        .message(INVALID_NOTIFICATION_PREFERENCE_ERROR.getDefaultMessage())
                        .build();
            }

            String[] parameters = queryString.split("&");
            if (parameters.length < 2) {
                throw CommandException.builder()
                        .status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .errorCode(MISSING_PARAMETERS_ERROR)
                        .message(MISSING_PARAMETERS_ERROR.getDefaultMessage())
                        .build();
            }

            String userReference = "";
            String notificationPreferences = "";
            for (String query : parameters) {
                String[] keyValue = query.split("=");
                if (keyValue.length < 2) {
                    continue;
                }
                if (keyValue[0].equalsIgnoreCase("user_reference")) {
                    userReference = keyValue[1];
                }
                if (keyValue[0].equalsIgnoreCase("notification_preferences")) {
                    notificationPreferences = keyValue[1];
                }
            }

            if (!StringUtils.hasText(userReference)) {
                throw CommandException.builder()
                        .status(HttpStatus.FORBIDDEN)
                        .errorCode(NO_PRINCIPAL)
                        .message(NO_PRINCIPAL.getDefaultMessage())
                        .build();
            }

            if (!StringUtils.hasText(notificationPreferences)) {
                throw CommandException.builder()
                        .status(HttpStatus.FORBIDDEN)
                        .errorCode(INVALID_NOTIFICATION_PREFERENCE_ERROR)
                        .message(INVALID_NOTIFICATION_PREFERENCE_ERROR.getDefaultMessage())
                        .build();
            }

            String[] notificationPreferenceArr = notificationPreferences.split(",");
            List<UserNotificationType> preferences = Arrays.stream(notificationPreferenceArr)
                    .map(x->UserNotificationType.valueOf(x.trim()))
                    .collect(Collectors.toList());
            this.userNotificationPreferenceService.addUserPreferences(userReference, preferences);

            return new UserPrincipal(userReference);
        } catch (Exception exception) {
            log.error("An error occurred while determining principal and preferences for user-notifications request", exception);
            throw new RuntimeException(exception);
        }
    }
}

package io.angularpay.usernotifications.adapter.outbound;

import io.angularpay.usernotifications.domain.UserNotificationHistory;
import io.angularpay.usernotifications.ports.outbound.PersistencePort;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MongoAdapter implements PersistencePort {

    private final UserNotificationHistoryRepository userNotificationHistoryRepository;

    @Override
    public UserNotificationHistory createRequest(UserNotificationHistory request) {
        request.setCreatedOn(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return userNotificationHistoryRepository.save(request);
    }

    @Override
    public UserNotificationHistory updateRequest(UserNotificationHistory request) {
        request.setLastModified(Instant.now().truncatedTo(ChronoUnit.SECONDS).toString());
        return userNotificationHistoryRepository.save(request);
    }

    @Override
    public Optional<UserNotificationHistory> findRequestByUserReference(String userReference) {
        return userNotificationHistoryRepository.findByUserReference(userReference);
    }

}

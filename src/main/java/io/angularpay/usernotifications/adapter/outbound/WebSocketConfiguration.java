package io.angularpay.usernotifications.adapter.outbound;

import io.angularpay.usernotifications.configurations.AngularPayConfiguration;
import io.angularpay.usernotifications.services.UserNotificationPreferenceService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

@Configuration
@EnableWebSocketMessageBroker
@RequiredArgsConstructor
public class WebSocketConfiguration implements WebSocketMessageBrokerConfigurer {

	private final AngularPayConfiguration configuration;
	private final UserNotificationPreferenceService userNotificationPreferenceService;

	@Override
	public void configureMessageBroker(MessageBrokerRegistry config) {
		config.enableSimpleBroker("/topic"); // what clients subscribe to
		config.setApplicationDestinationPrefixes("/publish"); // where clients publish messages
	}

	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		registry.addEndpoint("/user-notifications/connect") // where clients establish connection
				.setAllowedOrigins(this.configuration.getWebsocketOrigin())
				.setHandshakeHandler(new UserHandshakeHandler(this.userNotificationPreferenceService))
				.withSockJS();
	}

}

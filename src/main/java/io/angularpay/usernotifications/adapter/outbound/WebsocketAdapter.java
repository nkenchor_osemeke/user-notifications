package io.angularpay.usernotifications.adapter.outbound;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.ports.outbound.WebsocketPort;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class WebsocketAdapter implements WebsocketPort {

    private final SimpMessagingTemplate template;
    private final ObjectMapper mapper;

    @Override
    public void publishToUser(UserNotification userNotification) {
        try {
            String message = this.mapper.writeValueAsString(userNotification);
            log.info("message received for user: {}, message: {}", userNotification.getUserReference(), message);

            this.template.convertAndSendToUser(userNotification.getUserReference(),
                    "/topic/" + userNotification.getType().topic(), message);
        } catch (JsonProcessingException exception) {
            log.error("An error occurred while processing user-notifications request", exception);
        }
    }
}

package io.angularpay.usernotifications.adapter.outbound;

import io.angularpay.usernotifications.domain.UserNotificationHistory;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserNotificationHistoryRepository extends MongoRepository<UserNotificationHistory, String> {

    Optional<UserNotificationHistory> findByUserReference(String userReference);
}

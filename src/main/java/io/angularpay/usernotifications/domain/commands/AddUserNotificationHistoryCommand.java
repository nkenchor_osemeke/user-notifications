package io.angularpay.usernotifications.domain.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.usernotifications.adapter.outbound.MongoAdapter;
import io.angularpay.usernotifications.domain.Role;
import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.domain.UserNotificationHistory;
import io.angularpay.usernotifications.exceptions.ErrorObject;
import io.angularpay.usernotifications.models.AddUserNotificationHistoryCommandRequest;
import io.angularpay.usernotifications.validation.DefaultConstraintValidator;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class AddUserNotificationHistoryCommand extends AbstractCommand<AddUserNotificationHistoryCommandRequest, Void> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;

    public AddUserNotificationHistoryCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator) {
        super("AddUserNotificationHistoryCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
    }

    @Override
    protected String getResourceOwner(AddUserNotificationHistoryCommandRequest request) {
        return request.getAuthenticatedUser().getUserReference();
    }

    @Override
    protected Void handle(AddUserNotificationHistoryCommandRequest request) {
        Optional<UserNotificationHistory> optional = this.mongoAdapter.findRequestByUserReference(request.getUserNotification().getUserReference());

        if (optional.isEmpty()) {
            UserNotificationHistory userNotificationHistory = UserNotificationHistory.builder()
                    .userReference(request.getUserNotification().getUserReference())
                    .notificationHistory(Collections.singletonList(request.getUserNotification()))
                    .build();
            this.mongoAdapter.createRequest(userNotificationHistory);
        } else {
            UserNotificationHistory userNotificationHistory = optional.get();
            List<UserNotification> history = userNotificationHistory.getNotificationHistory();
            if (CollectionUtils.isEmpty(history)) {
                userNotificationHistory.setNotificationHistory(Collections.singletonList(request.getUserNotification()));
            } else {
                history.add(request.getUserNotification());
            }
            this.mongoAdapter.updateRequest(userNotificationHistory);
        }

        return null;
    }

    @Override
    protected List<ErrorObject> validate(AddUserNotificationHistoryCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.emptyList();
    }
}

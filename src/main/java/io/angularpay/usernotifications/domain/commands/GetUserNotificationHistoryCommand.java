package io.angularpay.usernotifications.domain.commands;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.angularpay.usernotifications.adapter.outbound.MongoAdapter;
import io.angularpay.usernotifications.domain.Role;
import io.angularpay.usernotifications.domain.UserNotification;
import io.angularpay.usernotifications.exceptions.CommandException;
import io.angularpay.usernotifications.exceptions.ErrorObject;
import io.angularpay.usernotifications.helpers.CommandHelper;
import io.angularpay.usernotifications.models.GetUserNotificationHistoryCommandRequest;
import io.angularpay.usernotifications.validation.DefaultConstraintValidator;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;

import static io.angularpay.usernotifications.exceptions.ErrorCode.REQUEST_NOT_FOUND;

@Service
public class GetUserNotificationHistoryCommand extends AbstractCommand<GetUserNotificationHistoryCommandRequest, List<UserNotification>> {

    private final MongoAdapter mongoAdapter;
    private final DefaultConstraintValidator validator;
    private final CommandHelper commandHelper;

    public GetUserNotificationHistoryCommand(
            ObjectMapper mapper,
            MongoAdapter mongoAdapter,
            DefaultConstraintValidator validator,
            CommandHelper commandHelper) {
        super("GetUserNotificationHistoryCommand", mapper);
        this.mongoAdapter = mongoAdapter;
        this.validator = validator;
        this.commandHelper = commandHelper;
    }

    @Override
    protected String getResourceOwner(GetUserNotificationHistoryCommandRequest request) {
        return commandHelper.getRequestOwner(request.getAuthenticatedUser().getUserReference());
    }

    @Override
    protected List<UserNotification> handle(GetUserNotificationHistoryCommandRequest request) {
        return this.mongoAdapter.findRequestByUserReference(request.getAuthenticatedUser().getUserReference())
                .orElseThrow(() -> CommandException.builder()
                        .status(HttpStatus.NOT_FOUND)
                        .errorCode(REQUEST_NOT_FOUND)
                        .message(REQUEST_NOT_FOUND.getDefaultMessage())
                        .build())
                .getNotificationHistory();
    }

    @Override
    protected List<ErrorObject> validate(GetUserNotificationHistoryCommandRequest request) {
        return this.validator.validate(request);
    }

    @Override
    protected List<Role> permittedRoles() {
        return Collections.singletonList(Role.ROLE_PLATFORM_ADMIN);
    }
}

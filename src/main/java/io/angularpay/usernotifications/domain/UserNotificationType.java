package io.angularpay.usernotifications.domain;

public enum UserNotificationType {
    ALL_NOTIFICATIONS("all-notifications"),
    INVESTMENT_VERIFIED("investment-verified"),
    SOLO_INVESTOR_ADDED("solo-investor-added"),
    PEER_INVESTOR_ADDED("peer-investor-added"),
    INVESTOR_BARGAIN_ADDED("investor-bargain-added"),
    BARGAIN_ACCEPTED("bargain-accepted"),
    BARGAIN_REJECTED("bargain-rejected"),
    INVESTOR_DELETED_BY_TTL("investor-deleted-by-ttl"),
    INVESTOR_DELETED_BY_SELF("investor-deleted-by-self"),
    INVESTMENT_COMPLETED("investment-completed"),
    INVESTMENT_MATURE("investment-mature"),
    INVESTMENT_CANCELLED("investment-cancelled");

    private final String topic;

    public String topic() {
        return this.topic;
    }

    UserNotificationType(String topic) {
        this.topic = topic;
    }
}

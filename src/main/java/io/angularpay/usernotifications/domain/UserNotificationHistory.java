package io.angularpay.usernotifications.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Version;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@Document("user_notification_history")
public class UserNotificationHistory {

    @Id
    private String id;
    @Version
    private int version;
    @JsonProperty("user_reference")
    private String userReference;
    @JsonProperty("notification_history")
    private List<UserNotification> notificationHistory;
    @JsonProperty("created_on")
    private String createdOn;
    @JsonProperty("last_modified")
    private String lastModified;
}

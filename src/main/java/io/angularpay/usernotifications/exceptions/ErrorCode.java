package io.angularpay.usernotifications.exceptions;

import lombok.Getter;

@Getter
public enum ErrorCode {
    INVALID_MESSAGE_JSON("An error occurred while parsing user-notifications request"),
    INVALID_MESSAGE_ERROR("The message format read from the given topic is invalid"),
    INVALID_USER_REFERENCE_ERROR("No valid user reference in user-notifications request"),
    INVALID_NOTIFICATION_TYPE_ERROR("Invalid notification type in user-notifications request"),
    ILLEGAL_NOTIFICATION_TYPE_ERROR("Illegal notification type in user-notifications request"),
    INVALID_NOTIFICATION_PREFERENCE_ERROR("Invalid notification preferences in user-notifications request"),
    MISSING_PARAMETERS_ERROR("There are missing required parameters in user-notifications request"),
    GENERIC_ERROR("Generic error occurred. See stacktrace for details"),
    VALIDATION_ERROR("The request has validation errors"),
    REQUEST_NOT_FOUND("The requested resource was NOT found"),
    AUTHORIZATION_ERROR("You do NOT have adequate permission to access this resource"),
    NO_PRINCIPAL("Principal identifier NOT provided", 500);

    private final String defaultMessage;
    private final int defaultHttpStatus;

    ErrorCode(String defaultMessage) {
        this(defaultMessage, 400);
    }

    ErrorCode(String defaultMessage, int defaultHttpStatus) {
        this.defaultMessage = defaultMessage;
        this.defaultHttpStatus = defaultHttpStatus;
    }
}

package io.angularpay.usernotifications.common;

public class Constants {
    public static final String USER_NOTIFICATIONS_TOPIC = "user-notifications";
    public static final String ERROR_SOURCE = "user-notifications";
}
